package com.example.ikomangsugiartha.pembelajaransim;

/**
 * Created by I Komang Sugiartha on 5/25/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;
public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "triviaQuiz";
    // tasks table name
    private static final String TABLE_QUEST = "quest";
    // tasks Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_QUES = "question";
    private static final String KEY_ANSWER = "answer"; //correct option
    private static final String KEY_OPTA= "opta"; //option a
    private static final String KEY_OPTB= "optb"; //option b
    private static final String KEY_OPTC= "optc"; //option c
    private SQLiteDatabase dbase;
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        dbase=db;
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_QUEST + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_QUES
                + " TEXT, " + KEY_ANSWER+ " TEXT, "+KEY_OPTA +" TEXT, "
                +KEY_OPTB +" TEXT, "+KEY_OPTC+" TEXT)";
        db.execSQL(sql);
        addQuestions();
        //db.close();
    }
    private void addQuestions()
    {
        Question q1=new Question("Yang bukan merupakan Marka" +
                " Lambang adalah:","Segi tiga", "Gambar", "Panas", "Panas");
        this.addQuestion(q1);
        Question q2=new Question(" Rambu dengan warna dasar kuning dengan " +
                "lambang atau tulisan Berwarna hitam merupakan: ", "Rambu petunjuk", " Rambu peringatan", "Rambu perintah", "Rambu petunjuk");
        this.addQuestion(q2);
        Question q3=new Question("Garis ganda yang terdiri dari " +
                "garis utuh dan garis putus-putus  termasuk: ","Marka membujur", "Marka melintang","Marka serong","Marka membujur");
        this.addQuestion(q3);
        Question q4=new Question("Apa kegunaan bahu " +
                "jalan?",	"Untuk pejalan Kaki", "Untuk berhenti dan parkir", " Untuk berhenti dalam keadaan darurat","Untuk berhenti dan parkir");
        this.addQuestion(q4);
        Question q5=new Question("SIM apakah Yang perlu dimiliki, bila anda akan " +
                "mengemudikan sepeda motor 300 CC dengan kereta samping?","SIM A","SIM C","SIM D","SIM C");
        this.addQuestion(q5);
        Question q6=new Question("Pencabutan SIM Seseorang oleh " +
                "hakim berakibat :","Orang tersebut untuk sementara waktu tidak boleh mengemudi sampai batas" +
                "putusan hakim berakhir","Orang tersebut untuk selamanya tidak boleh mengemudi","Orang tersebut dapat mengemudi asal mendapat SIM nya kembali dari Polisi","Orang tersebut untuk sementara waktu tidak boleh mengemudi sampai batas" +
                "putusan hakim berakhir");
        this.addQuestion(q6);
        Question q7=new Question("Helmisasi perlu dilakukan untuk mencegah cedera pada bagian kepala bila mengalami suatu kecelakaan, ketentuan yang harus diikuti adalah sbb :","Pengemudi saja yang harus memakai Helm","Pengemudi dan pembonceng harus memakai Helm","Pengemudi sepeda motor yang kurang dari 100 CC tidak" +
                " diharuskan memakai helm.","Pengemudi dan pembonceng harus memakai Helm");
        this.addQuestion(q7);
        Question q8=new Question("Tanda kendaraan bermotor dianggap sah apabila :","Dibuat sendiri oleh pemilik kendaraan bermotor, asalkan sesuai" +
                " dengan persyaratan undang undang","Dibuat sendiri sambil menunggu pelat nomor asli dari Polri","Diperoleh dari Polisi Lalu Lintas yang mengeluarkan STNK dan Pelat Nomor","Diperoleh dari Polisi Lalu Lintas yang mengeluarkan STNK dan Pelat Nomor");
        this.addQuestion(q8);
        Question q9=new Question("Bila hendak memboncengkan penumpang, sepeda motor harus dilengkapi dengan :","Tempat duduk, injakan kaki, dan pegangan untuk yang dibelakang","Injakan kaki dan pegangan untuk yang dibonceng","Tempat untuk barang yang dibonceng","Tempat duduk, injakan kaki, dan pegangan untuk yang dibelakang");
        this.addQuestion(q9);
        Question q10=new Question("Perubahan pada sepeda motor yang dapat menyebabkan STNKnya tidak Sah lagi adalah :","Memasang/mengganti mesin dengan kemampuan yang lebih besar","Mengganti kaca spion","Mengadakan perubahan pada sistim pembuangan","Memasang/mengganti mesin dengan kemampuan yang lebih besar");
        this.addQuestion(q10);
        Question q11=new Question("Apabila anda mendengar suara sirine yang kemungkinan dari pemadam Pemadam kebakaran, Polisi atau konvoi kendaraan. Anda diharuskan :","Menambah kecepatan","Menepi dan berhenti sampai konvoi lewat dan jalan aman","Menepi dan jalan terus, kalau memungkinkan mengikuti karaoke  kendaraan yang mengeluarkan bunyi/suara sirene","Menepi dan berhenti sampai konvoi lewat dan jalan aman");
        this.addQuestion(q11);
        Question q12=new Question("Sebagai pengemudi motor maka :","Untuk mencapai tujuan yang lebih cepat diperbolehkan zig zag diantara mobil, asalkan tidak terjadi kecelakaan.","Hanya boleh mendahului dari sebelah kanan apabila tidak ada rintangan lalu lintas lain dan dapat dilakukan dengan aman.","Diperbolehkan mendahului kendaraan lain dari kiri atau kanan","Hanya boleh mendahului dari sebelah kanan apabila tidak ada rintangan lalu lintas lain dan dapat dilakukan dengan aman.");
        this.addQuestion(q12);
        Question q13=new Question("Kendaraan apa yang berbahaya untuk didahului dan harus didahului hanya pada Jarak yang memadai untuk mendahului :","Kendaraan roda 2","Kendaraan roda 3","Kendaraan roda 4","Kendaraan roda 2");
        this.addQuestion(q13);
        Question q14=new Question("Pengemudi kendaraan bermotor yang terbukti beberapa kali melakukan pelanggaran lalu lintas atau pengemudi kendaraan bermotor yang terlibat sebagai tersangka dalam kasus kecelakaan lalu lintas dengan korban luka berat atau meninggal,maka Polri berwenang untuk : ","Membatalkan SIMnya","Melakukan uji ulang","Mencabut SIM nya","Melakukan uji ulang");
        this.addQuestion(q14);
        Question q15=new Question("Anda diwajibkan menggunakan pesawat penunjuk apabila :","Hendak berpapasan","Hendak mundur","Hendak beralih kejalur lain dijalan yang terbagi atas beberapa lajur","Hendak beralih kejalur lain dijalan yang terbagi atas beberapa lajur");
        this.addQuestion(q15);
        Question q16=new Question("Pada suatu ruas jalan ada 2 (dua) macam marka jalan berupa tanda garis membujur berwarna putih yang satu utuh dan yang lagi putus-putus, tanda garis mana yang harus dipatuhi oleh pengemudi :","Yang terdekat","Yang terjauh","Kedua-duanya","Yang terdekat");
        this.addQuestion(q16);
        Question q17=new Question("Surat Izin Mengemudi golongan C digunakankan untuk :","Mengemudikan sepeda motor yang dirancang mampu mencapai kecepatan lebih dari 40 Km per jam","Mengemudikan sepeda motor yang dirancang mampu mencapai kecepatan Tidak lebih dari 40 Km per jam","Pilihan a dan b salah","Mengemudikan sepeda motor yang dirancang mampu mencapai kecepatan lebih dari 40 Km per jam");
        this.addQuestion(q17);
        Question q18=new Question("Kendaraan yang digerakkan oleh peralatan teknik yang berada Pada kendaraan itu, disebut :","Sepeda motor","Kendaraan bermotor","Motor besar","Kendaraan bermotor");
        this.addQuestion(q18);
        Question q19=new Question("Apa kegunaan helm ?","Untuk melindungi pandangan pengendara, melindungi pengendara dari Panas dan hujan","Untuk melindungi kepala dari benturan atau gesekan yang mengakibatkan luka di kepala","Untuk menambah penampilan pengendara dan merupakan kelengkapan bagi sepeda motor","Untuk melindungi kepala dari benturan atau gesekan yang mengakibatkan luka di kepala");
        this.addQuestion(q19);
        Question q20=new Question("Pengemudi diharuskan memberikan isyarat dengan petunjuk arah yang berkedip pada waktu :","Akan berjalan atau akan mengubah arah ke kanan","Akan berjalan atau akan berhenti","Akan merubah arah ke kiri atau ke kanan","Akan merubah arah ke kiri atau ke kanan");
        this.addQuestion(q20);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUEST);
        // Create tables again
        onCreate(db);
    }
    // Adding new question
    public void addQuestion(Question quest) {
        //SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_QUES, quest.getQUESTION());
        values.put(KEY_ANSWER, quest.getANSWER());
        values.put(KEY_OPTA, quest.getOPTA());
        values.put(KEY_OPTB, quest.getOPTB());
        values.put(KEY_OPTC, quest.getOPTC());
        // Inserting Row
        dbase.insert(TABLE_QUEST, null, values);
    }
    public List<Question> getAllQuestions() {
        List<Question> quesList = new ArrayList<Question>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_QUEST;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Question quest = new Question();
                quest.setID(cursor.getInt(0));
                quest.setQUESTION(cursor.getString(1));
                quest.setANSWER(cursor.getString(2));
                quest.setOPTA(cursor.getString(3));
                quest.setOPTB(cursor.getString(4));
                quest.setOPTC(cursor.getString(5));
                quesList.add(quest);
            } while (cursor.moveToNext());
        }
        // return quest list
        return quesList;
    }
    public int rowcount()
    {
        int row=0;
        String selectQuery = "SELECT  * FROM " + TABLE_QUEST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        row=cursor.getCount();
        return row;
    }
}