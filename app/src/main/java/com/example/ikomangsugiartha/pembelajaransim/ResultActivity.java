package com.example.ikomangsugiartha.pembelajaransim;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.RatingBar;
import android.widget.TextView;
public class ResultActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //get rating bar object
        RatingBar bar=(RatingBar)findViewById(R.id.ratingBar1);
        bar.setNumStars(20);
        bar.setStepSize(0.5f);
        //get text view
        TextView t=(TextView)findViewById(R.id.textResult);
        //get score
        Bundle b = getIntent().getExtras();
        int score= b.getInt("score");
        //display score
        bar.setRating(score);
        switch (score)
        {
            case 1:t.setText("Nilai Anda 5");
                break;
            case 2:t.setText("Nilai Anda 10");
                break;
            case 3:t.setText("Nilai Anda 15");
                break;
            case 4:t.setText("Nilai Anda 20");
                break;
            case 5:t.setText("Nilai Anda 25");
                break;
            case 6:t.setText("Nilai Anda 30");
                break;
            case 7:t.setText("Nilai Anda 35");
                break;
            case 8:t.setText("Nilai Anda 40");
                break;
            case 9:t.setText("Nilai Anda 45");
                break;
            case 10:t.setText("Nilai Anda 50");
                break;
            case 11:t.setText("Nilai Anda 55");
                break;
            case 12:t.setText("Nilai Anda 60");
                break;
            case 13:t.setText("Nilai Anda 65");
                break;
            case 14:t.setText("Nilai Anda 70");
                break;
            case 15:t.setText("Nilai Anda 75");
                break;
            case 16:t.setText("Nilai Anda 80");
                break;
            case 17:t.setText("Nilai Anda 85");
                break;
            case 18:t.setText("Nilai Anda 90");
                break;
            case 19:t.setText("Nilai Anda 95");
                break;
            case 20:t.setText("Nilai Anda 100");
                break;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_result, menu);
        return true;
    }
}
