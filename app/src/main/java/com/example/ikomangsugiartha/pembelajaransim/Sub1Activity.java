package com.example.ikomangsugiartha.pembelajaransim;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Sub1Activity extends AppCompatActivity {

    private Button btnProsedur, btnPenggunaan, btnPersyaratan, btnLatihan, btnVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub1);

        btnProsedur = (Button)findViewById(R.id.button);
        btnPenggunaan = (Button)findViewById(R.id.button2);
        btnPersyaratan = (Button)findViewById(R.id.button3);
        btnLatihan = (Button)findViewById(R.id.button4);
        btnVideo = (Button)findViewById(R.id.button5);

        btnProsedur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Sub1Activity.this, Sub2Activity.class);
                startActivity(intent);

            }
        });

        btnPenggunaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Sub1Activity.this, Sub3Activity.class);
                startActivity(intent);

            }
        });

        btnPersyaratan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Sub1Activity.this, Sub4Activity.class);
                startActivity(intent);

            }
        });

        btnLatihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Sub1Activity.this, QuizActivity.class);
                startActivity(intent);

            }
        });

        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Sub1Activity.this, Sub6Activity.class);
                startActivity(intent);

            }
        });


    }
}
