package com.example.ikomangsugiartha.pembelajaransim;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.VideoView;

public class Sub6Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub6);
        final VideoView videoView =
                (VideoView) findViewById(R.id.videoView);
        videoView.setVideoPath(
                "http://sugiartha.com/wp-content/uploads/2016/04/rambu.mp4");
        videoView.start();
    }
}
